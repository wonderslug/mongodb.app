//
//  MDBApplicationMover.h
//  MongoDB
//
// Modified by Brian Towles (brian@towles.com) for MongoDB
//
//  Original by Jakob Egger on 18.12.13.
//
//

#import <Foundation/Foundation.h>

@interface MDBApplicationMover : NSObject

@property NSString *targetFolder;
@property NSString *targetName;

+(id)sharedApplicationMover;
-(void)validateApplicationPath;


@end
