//
//  PreferencesWindowController.m
//  MongoDB
//
// Modified by Brian Towles (brian@towles.com) for MongoDB
//
//  Original by Jakob Egger on 18.12.13.
//
//

#import "PreferenceWindowController.h"
#import <ServiceManagement/ServiceManagement.h>
#import "MongoDBConstants.h"
#import "MongoDBServer.h"

@implementation PreferenceWindowController

+(PreferenceWindowController*)sharedController {
	static PreferenceWindowController* sharedController = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedController = [[PreferenceWindowController alloc] initWithWindowNibName:@"PreferenceWindow"];
	});
	return sharedController;
}

-(void)windowDidLoad {
	[self configureLoginItemButton];
}

-(void)configureLoginItemButton {
	BOOL loginItemEnabled = NO;
	NSArray *jobs = (__bridge_transfer NSArray *)SMCopyAllJobDictionaries(kSMDomainUserLaunchd);
	for (NSDictionary *job in jobs) {
		if ([[job valueForKey:@"Label"] isEqualToString:@"org.mongodb.MongoDBHelper"]) {
			loginItemEnabled = YES;
			break;
		}
	}
	[loginItemCheckbox setState: loginItemEnabled ? NSOnState : NSOffState];
	
	BOOL loginItemSupported = [[NSBundle mainBundle].bundlePath isEqualToString:@"/Applications/MongoDB.app"];
	if (loginItemSupported) {
		loginItemCheckbox.target = self;
		loginItemCheckbox.action = @selector(toggleLoginItem:);
	} else {
		loginItemCheckbox.enabled = NO;
	}
}

-(IBAction)toggleLoginItem:(id)sender {
	BOOL loginItemEnabled = (loginItemCheckbox.state == NSOnState);
    
    NSURL *helperApplicationURL = [[NSBundle mainBundle].bundleURL URLByAppendingPathComponent:@"Contents/Library/LoginItems/MongoDBHelper.app"];
    if (LSRegisterURL((__bridge CFURLRef)helperApplicationURL, true) != noErr) {
        NSLog(@"LSRegisterURL Failed");
    }
    
    BOOL stateChangeSuccessful = SMLoginItemSetEnabled(CFSTR("org.mongodb.MongoDBHelper"), loginItemEnabled);
	if (!stateChangeSuccessful) {
        NSError *error = [NSError errorWithDomain:@"org.mongodb.MongoDB" code:1 userInfo:@{ NSLocalizedDescriptionKey: @"Failed to set login item."}];
		[self presentError:error modalForWindow:self.window delegate:nil didPresentSelector:NULL contextInfo:NULL];
		loginItemCheckbox.state = loginItemEnabled ? NSOffState : NSOnState;
    }
}


-(IBAction)openDataDirectory:(id)sender;
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL fileURLWithPath:[[NSUserDefaults standardUserDefaults] stringForKey:kMongoDBDataDirectoryPreferenceKey]]];
}

-(IBAction)chooseDataDirectory:(id)sender;
{
	NSOpenPanel* dataDirPanel = [NSOpenPanel openPanel];
	dataDirPanel.canChooseDirectories = YES;
	dataDirPanel.canChooseFiles = NO;
	dataDirPanel.canCreateDirectories = YES;
	dataDirPanel.directoryURL = [NSURL fileURLWithPath:[[NSUserDefaults standardUserDefaults] stringForKey:kMongoDBDataDirectoryPreferenceKey]];
	[dataDirPanel beginSheetModalForWindow:self.window completionHandler:^(NSInteger result) {
		if (result==NSFileHandlingPanelOKButton) {
			[[NSUserDefaults standardUserDefaults] setObject:dataDirPanel.URL.path forKey:kMongoDBDataDirectoryPreferenceKey];
		}
	}];
}

-(IBAction)resetDataDirectory:(id)sender;
{
	[[NSUserDefaults standardUserDefaults] setObject:[MongoDBServer standardDatabaseDirectory] forKey:kMongoDBDataDirectoryPreferenceKey];
}

-(BOOL)windowShouldClose:(NSWindow*)window {
	BOOL controlDidResign = [self.window makeFirstResponder:nil];
	if (!controlDidResign) NSBeep();
	[[NSUserDefaults standardUserDefaults] synchronize];
	return controlDidResign;
}


@end
