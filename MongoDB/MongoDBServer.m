// MongoDBServer.m
//
// Modified by Brian Towles (brian@towles.com) for MongoDB
// Original Copyright Below
//
// Created by Mattt Thompson (http://mattt.me/)
// Copyright (c) 2012 Heroku (http://heroku.com/)
// 
// Portions Copyright (c) 1996-2012, The PostgreSQL Global Development Group
// Portions Copyright (c) 1994, The Regents of the University of California
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose, without fee, and without a written agreement
// is hereby granted, provided that the above copyright notice and this
// paragraph and the following two paragraphs appear in all copies.
//
// IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
// DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
// LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
// EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.
//
// THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN
// "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO
// PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

#import <xpc/xpc.h>
#import "MongoDBServer.h"
#import "NSFileManager+DirectoryLocations.h"

#define xstr(a) str(a)
#define str(a) #a

@interface MongoDBServer()

@property BOOL isRunning;

@end

@implementation MongoDBServer {
    __strong NSTask *_mongoDBTask;
    NSUInteger _port;
    
    xpc_connection_t _xpc_connection;
}

+(NSString*)standardDatabaseDirectory {
	return [[[NSFileManager defaultManager] applicationSupportDirectory] stringByAppendingFormat:@"/var-%s", xstr(MDB_MAJOR_VERSION)];
}

+(MongoDBDataDirectoryStatus)statusOfDataDirectory:(NSString*)dir {
    BOOL isDir;
    
	if (![[NSFileManager defaultManager] fileExistsAtPath:dir]) {
		return MongoDBDataDirectoryMissing;
	}
    else if ([[NSFileManager defaultManager] fileExistsAtPath:dir isDirectory:&isDir] && !isDir)
    {
        return MongoDBDataDirectoryIncompatible;
    }
	return MongoDBDataDirectoryCompatible;
}

+(NSString*)existingDatabaseDirectory {
	// This function tries to locate existing data directories with the same version
	// It returns the first matching data directory
	NSArray *applicationSupportDirectories = @[
											   [[NSFileManager defaultManager] applicationSupportDirectory]
											   ];
	NSArray *dataDirNames = @[
							  @"var",
							  [NSString stringWithFormat:@"var-%s",xstr(MDB_MAJOR_VERSION)]
							  ];
	for (NSString *applicationSupportDirectory in applicationSupportDirectories) {
		for (NSString *dataDirName in dataDirNames) {
			NSString *dataDirectoryPath = [applicationSupportDirectory stringByAppendingPathComponent:dataDirName];
			MongoDBDataDirectoryStatus status = [self statusOfDataDirectory:dataDirectoryPath];
			if (status == MongoDBDataDirectoryCompatible) {
				return dataDirectoryPath;
			}
		}
	}
	return nil;
}

+(MongoDBServer *)sharedServer {
    static MongoDBServer *_sharedServer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
		NSString *binDirectory = [[NSBundle mainBundle].bundlePath stringByAppendingFormat:@"/Contents/Versions/%s/bin",xstr(MDB_MAJOR_VERSION)];
		NSString *databaseDirectory = [[NSUserDefaults standardUserDefaults] stringForKey:kMongoDBDataDirectoryPreferenceKey];
		if (!databaseDirectory || [self statusOfDataDirectory:databaseDirectory] == MongoDBDataDirectoryIncompatible) {
			databaseDirectory = [self existingDatabaseDirectory];
		}
		if (!databaseDirectory) {
			databaseDirectory = [self standardDatabaseDirectory];
		}
		[[NSUserDefaults standardUserDefaults] setObject:databaseDirectory forKey:kMongoDBDataDirectoryPreferenceKey];
        _sharedServer = [[MongoDBServer alloc] initWithExecutablesDirectory:binDirectory databaseDirectory:databaseDirectory];
    });
    
    return _sharedServer;
}

- (id)initWithExecutablesDirectory:(NSString *)executablesDirectory
                 databaseDirectory:(NSString *)databaseDirectory
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _binPath = executablesDirectory;
    _varPath = databaseDirectory;
    _port    = kMongoDBAppDefaultPort;
   
    NSString *conf = [_varPath stringByAppendingPathComponent:@"mongod.conf"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:conf]) {
        const char *t = [[NSString stringWithContentsOfFile:conf encoding:NSUTF8StringEncoding error:nil] UTF8String];
        for (int i = 0; t[i]; i++) {
            if (t[i] == '#')
                while (t[i] != '\n' && t[i]) i++;
            else if (strncmp(t + i, "port ", 5) == 0) {
                if (sscanf(t + i + 5, "%*s %ld", &_port) == 1)
                    break;
            }
        }
    }

    _xpc_connection = xpc_connection_create("org.mongodb.mongod-service", dispatch_get_main_queue());
	xpc_connection_set_event_handler(_xpc_connection, ^(xpc_object_t event) {
        xpc_dictionary_apply(event, ^bool(const char *key, xpc_object_t value) {
			return true;
		});
	});
	xpc_connection_resume(_xpc_connection);
    
    return self;
}

- (BOOL)startWithTerminationHandler:(void (^)(NSUInteger status))completionBlock
{
    [self stopWithTerminationHandler:nil];
    
    MongoDBDataDirectoryStatus dataDirStatus = [MongoDBServer statusOfDataDirectory:_varPath];
	
    if (dataDirStatus==MongoDBDataDirectoryMissing) {
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:_varPath
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create directory error: %@", error);
        }
        dataDirStatus =[MongoDBServer statusOfDataDirectory:_varPath];
    }
	
    if (dataDirStatus==MongoDBDataDirectoryCompatible) {
        [self executeCommandNamed:[_binPath stringByAppendingPathComponent:@"mongod"] arguments:[NSArray arrayWithObjects:@"--fork", @"--dbpath", _varPath, @"--logpath", [NSString stringWithFormat:@"%@/mongo.log", _varPath], @"--logappend", @"--pidfilepath", [NSString stringWithFormat:@"%@/mongod.pid", _varPath], @"--bind_ip", @"127.0.0.1", nil] terminationHandler:^(NSUInteger status) {
            self.isRunning = (status == 0);
            if (completionBlock) {
                completionBlock(status);
            }
        }];
    }
	else {
		if (completionBlock) {
			completionBlock(1);
		}
	}
    
    
    return YES;
}

- (BOOL)stopWithTerminationHandler:(void (^)(NSUInteger status))terminationHandler {
    NSString *pidPath = [_varPath stringByAppendingPathComponent:@"mongod.pid"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pidPath]) {
        NSString *pid = [[[NSString stringWithContentsOfFile:pidPath encoding:NSUTF8StringEncoding error:nil] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] objectAtIndex:0];        
        [self executeCommandNamed:@"/bin/kill" arguments:[NSArray arrayWithObjects:pid, nil] terminationHandler:terminationHandler];
        [[NSFileManager defaultManager] removeItemAtPath:pidPath error:nil];
    }
    
    return YES;
}

- (void)executeCommandNamed:(NSString *)command 
                  arguments:(NSArray *)arguments
         terminationHandler:(void (^)(NSUInteger status))terminationHandler
{
	xpc_object_t message = xpc_dictionary_create(NULL, NULL, 0);

    xpc_dictionary_set_string(message, "command", [command UTF8String]);
    
    xpc_object_t args = xpc_array_create(NULL, 0);
    [arguments enumerateObjectsUsingBlock:^(id argument, NSUInteger idx, BOOL *stop) {
        xpc_array_set_value(args, XPC_ARRAY_APPEND, xpc_string_create([argument UTF8String]));
    }];
    xpc_dictionary_set_value(message, "arguments", args);
    
    xpc_connection_send_message_with_reply(_xpc_connection, message, dispatch_get_main_queue(), ^(xpc_object_t object) {
        NSLog(@"%lld %s: Status %lld", xpc_dictionary_get_int64(object, "pid"), xpc_dictionary_get_string(object, "command"), xpc_dictionary_get_int64(object, "status"));
        
        if (terminationHandler) {
            terminationHandler(xpc_dictionary_get_int64(object, "status"));
        }
    });
}

@end
