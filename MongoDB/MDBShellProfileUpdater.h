//
//  MDBShellProfileUpdater.h
//  MongoDB
//
// Modified by Brian Towles (brian@towles.com) for MongoDB
//
//  Original by Jakob Egger on 18.12.13.
//
//

#import <Foundation/Foundation.h>

@interface MDBShellProfileUpdater : NSObject

@property NSArray *profilePaths;
@property NSArray *oldPaths;
@property NSString *currentPath;


+(MDBShellProfileUpdater*)sharedUpdater;
-(void)checkProfiles;

@end
