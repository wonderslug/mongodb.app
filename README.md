# MongoDB.app

This is a wrapper for the open source MongoDB mac distribution by Brian Towles

## Description
It is a modified version of the [Postgres.app](http://postgresapp.com/) wrapper.  It has a built in distribution of MongoDB (2.6.1) and runs in the status bar. 

## Rational
This is intended to show a bit of my skill set.  I am able to adapt to multiple languages and environments fairly quickly. I do not have a lot of experience with Objective-C but I was able to adapt this with about 6 hours work over a weekend.  It still needs some cleanup but is working nicely to start with. 

As well this is something that could be a good addition to the MongoDB open source distributions.  The current methods of installation are manual or brew type installations.  This sort of wrapper allows MongoDB to be easily installed and used for development in a fraction of the time.  It would target web developers and newer developers with little to no command line experience allowing MongoDB to be distributed to a wider audience.  As well this could be submitted for free to the Mac App Store in the Developer Tools section to allow very easy access for Mac developers.  There are currently no MongoDB apps on the the Mac App store.

## Download

You can get the compiled version of this from [the downloads section](https://bitbucket.org/wonderslug/mongodb.app/downloads/);

## How To Build

This is a standard XCode project.  You do need to take the current release of MongoDB and untar it in the root directory of the project.  It should be under a directory like `mongodb-osx-x86_64-2.6.1`  If you use a different version, make sure you change the version number in the User-Defined section of the project build settings.  Building the MongoDB scheme will build the and copy the MongoDB binary executables from the distribution to the application for usage in running the server and the client.
